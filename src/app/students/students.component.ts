import { Component, OnInit } from '@angular/core';
import { Student } from '../entity/student';
import { StudentDataImplService } from '../service/student-data-impl.service';
import { StudentService } from '../service/student-service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
  students: Student[];

  averageGpa(): number {
    let sum = 0;
    if(this.students){
      for (let student of this.students) {
        sum += student.gpa;
      }
      return sum / this.students.length;
    }
    return 0;
   
  }

  upQuantity(student){
    student.penAmount++;
  }

  downQuantity(student){
    if(student.penAmount - 1 < 0 ){
      student.penAmount = 0;
    }else{
      student.penAmount--;
    }
  }

  getCoord(event: MouseEvent){
    console.log(event.clientX + ", "+ event.clientY);
  }


  reset(student){
    student.penAmount = 0;
  }

  constructor(private studentService: StudentService) { }

  ngOnInit() {
    this.studentService.getStudents().subscribe(
      students => {
        this.students = students;
      }
    );
  }

}
