import { TestBed } from '@angular/core/testing';

import { StudentFileImplService } from './student-file-impl.service';

describe('StudentFileImplService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StudentFileImplService = TestBed.get(StudentFileImplService);
    expect(service).toBeTruthy();
  });
});
