import { TestBed } from '@angular/core/testing';

import { PeopleFileImplService } from './people-file-impl.service';

describe('PeopleFileImplService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PeopleFileImplService = TestBed.get(PeopleFileImplService);
    expect(service).toBeTruthy();
  });
});
